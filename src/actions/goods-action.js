import {GET_ALL_GOODS, GET_GOOD_IMAGE, GET_GOOD_NAME, GET_GOOD_PRICE, GET_GOOD_UNIT} from "../constants/ActionType";
import {getAllData} from "../utils/RequestUtils";
import {GOOD_URL} from "../constants/url";


export const getAllGoods = () => (dispatch) => {
  getAllData(GOOD_URL)
    .then(response => {
        dispatch({
          type: GET_ALL_GOODS,
          allGoods: response
        })
      }
    );
}

export const getName = (name) => (dispatch) => {
  dispatch({
    type: GET_GOOD_NAME,
    name: name
  })
};

export const getPrice = (price) => (dispatch) => {
  dispatch({
    type: GET_GOOD_PRICE,
    price: price
  })
};

export const getUnit = (unit) => (dispatch) => {

  dispatch({
    type: GET_GOOD_UNIT,
    unit: unit
  })
};

export const getImageUrl = (imgUrl) => (dispatch) => {
  dispatch({
    type: GET_GOOD_IMAGE,
    imgUrl: imgUrl
  })
};