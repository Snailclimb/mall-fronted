import React, {Component} from "react";
import "./App.less";
import Home from "./components/home";
import "./styles/index.less";
import {Route, Switch} from "react-router";
import {BrowserRouter as Router} from "react-router-dom";
import AddGoods from "./components/add-goods";

class App extends Component {
  render() {
    return (
      <Router>
        <Switch>
          <Route exact path="/" component={Home} />
          <Route exact path="/addGoods" component={AddGoods} />
          <Route exact path="/" component={Home} />
        </Switch>
      </Router>
    );
  }
}

export default App;
