import React, {Component} from 'react';
import {postData} from "../utils/RequestUtils";
import {ORDER_URL} from "../constants/url";

class Goods extends Component {

  constructor(props) {
    super(props);
    this.updateOrders = this.updateOrders.bind(this);
  }


  render() {
    return (
      <div>
        <button onClick={this.updateOrders}>+</button>
      </div>
    );
  }

  updateOrders() {
    console.log(this.props.good)
    postData(ORDER_URL, this.props.good)
      .then(() => console.log("添加订单成功"));
  }
}

export default Goods;