import React, {Component} from 'react';
import {bindActionCreators} from "redux";
import {getAllGoods} from "../actions/goods-action";
import {connect} from "react-redux";
import Goods from "./goods";

class ShowAllGoods extends Component {

  componentDidMount() {
    this.props.getAllGoods();
    this.getNotesList = this.getNotesList.bind(this);
  }

  render() {
    console.log(this.props.allGoods);
    return (
      <div>
        {this.getNotesList()}
      </div>
    );
  }

  getNotesList() {
    return this.props.allGoods.map((good, index) => (
      <div className="home-notes-display" key={index}>
        <span>{good.name}</span>
        <Goods good={good}></Goods>
      </div>
    ));
  }
}


const mapStateToProps = state => ({
  allGoods: state.goods.allGoods
});

const mapDispatchToProps = dispatch =>
  bindActionCreators({getAllGoods}, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ShowAllGoods);