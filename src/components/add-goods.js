import React, {Component} from 'react';
import {postData} from "../utils/RequestUtils";
import {GOOD_URL} from "../constants/url";
import {bindActionCreators} from "redux";
import {getImageUrl, getName, getPrice, getUnit} from "../actions/goods-action";
import {connect} from "react-redux";

class AddGoods extends Component {
  constructor(props) {
    super(props);
    this.handleNameChange = this.handleNameChange.bind(this);
    this.handlePriceChange = this.handlePriceChange.bind(this);
    this.handleImageUrlChange = this.handleImageUrlChange.bind(this);
    this.handleUnitChange = this.handleUnitChange.bind(this);
    this.addGoods = this.addGoods.bind(this);
  }

  render() {
    return (
      <div className={'add-good-form'}>
        <p>名称</p>
        <br/>
        <input placeholder="名称" className={'good-input'}
               onChange={this.handleNameChange}
        />
        <p>价格</p>
        <br/>
        <input placeholder="价格" className={'good-input'} onChange={this.handlePriceChange}/>
        <p>单位</p>
        <input placeholder="单位" className={'good-input'} onChange={this.handleUnitChange}/>
        <p>图片</p>
        <br/>
        <input placeholder="图片" className={'good-input'} onChange={this.handleImageUrlChange}/>
        <br/>
        <button onClick={this.addGoods}>submit</button>
      </div>
    )
  }

  handleNameChange(e) {
    this.props.getName(e.target.value);
  }

  handlePriceChange(e) {
    this.props.getPrice(e.target.value);
  }

  handleUnitChange(e) {
    this.props.getUnit(e.target.value);
  }

  handleImageUrlChange(e) {
    this.props.getImageUrl(e.target.value);
  }

  addGoods() {
    const data = {
      name: this.props.name,
      price: Number(this.props.price),
      unit: this.props.unit,
      imageUrl: this.props.imgUrl
    };
    console.log(data)
    postData(GOOD_URL, data).then(() => this.props.history.push("/"));
  }
}

const mapStateToProps = state => ({
  name: state.goods.name,
  price: state.goods.price,
  unit: state.goods.unit,
  imgUrl: state.goods.imgUrl
});

const mapDispatchToProps = dispatch =>
  bindActionCreators({getName, getPrice, getUnit, getImageUrl}, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AddGoods);