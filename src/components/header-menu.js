import React, {Component} from 'react';
import {Icon, Menu} from "antd";
import {NavLink} from "react-router-dom";

class HeaderMenu extends Component {
  render() {
    return (
      <header>
        <div className={'nav-link-item'}>
          <NavLink className={'header-link'} to={{
            pathname: '/addGoods'
          }}>
            商城
          </NavLink>
        </div>
        <div className={'nav-link-item'}>
          <NavLink className={'header-link'} to={{
            pathname: '/addGoods'
          }}>
            订单
          </NavLink>
        </div>
        <div className={'nav-link-item'}>
          <NavLink className={'header-link'} to={{
            pathname: '/addGoods'
          }}>
            添加商品
          </NavLink>
        </div>
      </header>
    );
  }
}

export default HeaderMenu;