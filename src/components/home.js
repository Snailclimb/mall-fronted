import React, {Component} from 'react';
import HeaderMenu from "./header-menu";
import AddGoods from "./add-goods";
import ShowAllGoods from "./show-all-goods";

class Home extends Component {
  render() {
    return (
      <div>
        <HeaderMenu>
        </HeaderMenu>
        <ShowAllGoods></ShowAllGoods>
      </div>
    );
  }
}

export default Home;