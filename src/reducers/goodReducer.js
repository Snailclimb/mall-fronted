import {GET_ALL_GOODS, GET_GOOD_IMAGE, GET_GOOD_NAME, GET_GOOD_PRICE, GET_GOOD_UNIT} from "../constants/ActionType";

const initState = {
  allGoods: [],
  name: '',
  unit: '',
  price: '',
  imgUrl: '',
};

export default (state = initState, action) => {
  switch (action.type) {
    case GET_ALL_GOODS:
      return {
        ...state,
        allGoods: action.allGoods
      };
    case GET_GOOD_NAME:
      return {
        ...state,
        name: action.name
      };
    case GET_GOOD_PRICE:
      return {
        ...state,
        price: action.price
      };
    case GET_GOOD_UNIT:
      return {
        ...state,
        unit: action.unit
      };
    case GET_GOOD_IMAGE:
      return {
        ...state,
        imgUrl: action.imgUrl
      };
    default:
      return state;
  }
};
