import {combineReducers} from "redux";
import goodReducer from "./goodReducer";

const reducers = combineReducers({
  goods: goodReducer
});
export default reducers;
