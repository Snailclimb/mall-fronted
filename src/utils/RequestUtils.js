import axios from "axios";

export function getAllData(url) {
  return axios
    .get(`${url}`)
    .catch(error => console.log(error))
    .then(response => response.data.content);
}

export function deleteByID(url, id) {
  return axios
    .delete(`${url}/${id}`)
    .catch(error => console.log(error))
}

export function postData(url, data) {
  return axios
    .post(
      `${url}`, data,
      {
        headers: {
          'Access-Control-Allow-Origin': '*'
        }
      }
    )
    .catch(error => console.log(error))
}