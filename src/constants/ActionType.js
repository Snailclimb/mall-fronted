export const GET_ALL_GOODS = 'GET_ALL_GOODS';
export const GET_GOOD_NAME = 'GET_GOOD_NAME';
export const GET_GOOD_IMAGE = 'GET_GOOD_IMAGE';
export const GET_GOOD_UNIT = 'GET_GOOD_UNIT';
export const GET_GOOD_PRICE= 'GET_GOOD_PRICE';